<?php 

namespace App\Controllers;

include_once (dirname(__FILE__,2).'/database/dS.php');

use Illuminate\Database\Capsule\Manager as Capsule;

use App\Model\{Requestapp_Model};

class Index_Controller {
	
	function indexHome() {
        
        $DataInit = Capsule::table('tbldatainit')->get();
        $_POST = $DataInit;           
        $Requestapp = new Requestapp_Model;       
        //$Process->setTable('tblpruebas');
     
        $Requestapp->path_tblrequestapp = $_REQUEST['PATH'];
        $Requestapp->ipclient_tblrequestapp = $_REQUEST['IPCLIENT'];
        $Requestapp->time_tblrequestapp = $_REQUEST['REQUEST_TIME'];
        $Requestapp->idramd_tblrequestapp = $_REQUEST['ID_CLIENT'];
        $Requestapp->save(); 
        unset($_REQUEST);
        include_once (dirname(__FILE__,2).'/view/layout/app_View.php');
        
    }

    function validDashboard() {

        $usuSystem = Capsule::table('tblusersystem')->where('mail_tblusersystem', $_REQUEST['email'])->get();
        $redirect = password_verify($_REQUEST['password'], $usuSystem[0]->{'password_tblusersystem'});

        if ($redirect) {
            return header('Location: /dashboard');
        }else{
            return header('Location: /');
        }
    }
}

?>