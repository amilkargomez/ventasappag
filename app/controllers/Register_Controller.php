<?php 

namespace App\Controllers;

include_once (dirname(__FILE__,2).'/database/dS.php');

use App\Model\{UserSys_Model};
use App\Controllers\{Valid_Controller};

class Register_Controller {
    
    function registrarHome() {

        include_once (dirname(__FILE__,2).'/view/register/register_View.php');	

    }

    function registrarUsuario() {

        $error = array();
        $nomCompl = '';
        $correoE = '';
        $nomComplOld = $_POST['Nombre_Completo'];
        $correoEOld = $_POST['Email'];
        $pass = '';

        $validData = new Valid_Controller;

        if ( empty(!$_POST['Nombre_Completo'])  ) {
            $nomCompl = $validData->ValidString($_POST['Nombre_Completo']);      
            if ($nomCompl == 1) {
                $nomCompl = $_POST['Nombre_Completo'];
            }else{
                $error=['Error en el Nombre, No Cumple con el Estandar'];     
            }    
        }else{
            $error=['Error en el Nombre Vacio'];   
        }

        if ( empty(!$_POST['Email']) ) {
            $correoE = $validData->validMail($_POST['Email']);       
            if ($correoE == 1) {
                $correoE = $_POST['Email'];
            }else{
                array_push($error, 'Error el Correo Electronico, No Cumple con el Estandar');     
            }    
        }else{
            array_push($error,'Error en el Correo Electronico');   
        }

        if ( empty(!$_POST['Password_one']) && empty(!$_POST['Password_two']) ) {

            if ($_POST['Password_one'] == $_POST['Password_two']) {

                $pass = $validData->validPassword($_POST['Password_one']);       
                if ($pass == 1) {
                    $pass = $_POST['Password_one'];
                }else{
                    array_push($error, 'Error la Contraseña No Cumple con el Estandar');     
                } 
                
            }else {
                array_push($error, 'Error en la Contraseña, No coinciden');
            }
       
        }else{
            array_push($error,'Error en la Contraseña');   
        }    

        if ( count($error)>=1 ) {
            unset($_REQUEST,$_POST);
            $_POST=$error;
            $_POST['old'] = [0=>$nomComplOld,1=>$correoEOld];
            include_once (dirname(__FILE__,2).'/view/register/register_View.php');	
        }else{
            $options = ['memory_cost' => 1<<14, 'time_cost' => 20, 'threads' => 10];
            $protegerpass = password_hash($pass, PASSWORD_ARGON2I, $options);
            $Usersys = new UserSys_Model;     
            $Usersys->name_tblusersystem = $nomCompl;
            $Usersys->mail_tblusersystem = $correoE;
            $Usersys->password_tblusersystem = $protegerpass;
            $Usersys->save(); 
            unset($_REQUEST,$_POST);
            header('Location: /regitersok');
        }
    }

}

?>