<?php 

namespace App\Controllers;

class Valid_Controller {
	
	function ValidString($data) {

        $data = utf8_encode($data);
        $dataInt = str_replace(
            array('*',';','"','FROM','from',',','SELECT','DROP','TRUNCATE','select','drop','truncate','script','SCRIPT'),
            array('','','','','','','','','','','',''),
            $data);

        strip_tags($dataInt);
        return preg_match('/^[a-z ]+$/i', $dataInt); 

    }

    function validMail($data){

        $data = utf8_encode($data);
        $dataInt = str_replace(
            array('*',';','"','FROM','from',',','SELECT','DROP','TRUNCATE','select','drop','truncate','script','SCRIPT'),
            array('','','','','','','','','','','',''),
            $data);
        strip_tags($dataInt);
        return preg_match('/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/', $dataInt); 
        
    }

    function validPassword($data){

        $data = utf8_encode($data);
        $dataInt = str_replace(
            array('*',';','"','FROM','from',',','SELECT','DROP','TRUNCATE','select','drop','truncate','script','SCRIPT'),
            array('','','','','','','','','','','',''),
            $data);
        strip_tags($dataInt);
        if ( strlen($dataInt)>=4 ) {
            $dataInt=1;
        }else{
            $dataInt=0;
        }

        return $dataInt; 
    }
    	
}

?>