<?php 

namespace App\Controllers;

include_once (dirname(__FILE__,2).'/database/dS.php');

use Illuminate\Database\Capsule\Manager as Capsule;

use App\Model\{Requestapp_Model};

class Indexs_Controller {
	
	function indexHome() {

        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])){
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        }else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else if(isset($_SERVER['HTTP_X_FORWARDED'])){
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        }else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])){
            $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        }else if(isset($_SERVER['HTTP_FORWARDED_FOR'])){
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        }else if(isset($_SERVER['HTTP_FORWARDED'])){
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        }else if(isset($_SERVER['REMOTE_ADDR'])){
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        }else{
            $ipaddress = 'UNKNOWN';
        }
    
        $idRamd = rand(5, 88888);
    
        $requestArray = array('PATH' => $_SERVER['PATH'], 'IPCLIENT' => $ipaddress, 
                            'REQUEST_TIME' => $_SERVER['REQUEST_TIME'], 
                                'ID_CLIENT' => $idRamd);
                  
        $Requestapp = new Requestapp_Model;       
        //$Process->setTable('tblpruebas');
        $Requestapp->path_tblrequestapp = $requestArray['PATH'];
        $Requestapp->ipclient_tblrequestapp = $requestArray['IPCLIENT'];
        $Requestapp->time_tblrequestapp = $requestArray['REQUEST_TIME'];
        $Requestapp->idramd_tblrequestapp = $requestArray['ID_CLIENT'];
        $Requestapp->save(); 
        unset($requestArray);
        
    }

    //Ojo Cargar variables en SESION
    function validDashboard() {
        $DataInit = Capsule::table('tbldatainit')->get();
        $_POST = $DataInit; 
        $usuSystem = Capsule::table('tblusersystem')->where('mail_tblusersystem', $_REQUEST['email'])->get();
        $redirect = password_verify($_REQUEST['password'], $usuSystem[0]->{'password_tblusersystem'});
        $this->indexHome();
        $usuSystem = array('name_tblusersystem' => $usuSystem[0]->{'name_tblusersystem'},
                            'mail_tblusersystem' => $usuSystem[0]->{'mail_tblusersystem'},
                            'level_tblusersystem' => $usuSystem[0]->{'level_tblusersystem'},
                            'state_tblusersystem' => $usuSystem[0]->{'state_tblusersystem'}
                          );

        if ( ( $redirect == TRUE ) && ( $usuSystem['state_tblusersystem'] == 1 ) ) {
            return include_once (dirname(__FILE__,2).'/view/layout/app_View.php');
        }else{
            return header('Location: /');
        }
    }
}

?>