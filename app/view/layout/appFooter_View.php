<?php

namespace View\Layout;

use Illuminate\Database\Capsule\Manager as Capsule;

class appFooter_View {

    private $footerPart1One;
    private $footerPart2One;
    private $DataInit;
    

    function __construct($case)
    {
        switch ($case) {
            case 'one':
                $this->SetFooterPart1One();
                $this->SetFooterPart2One();   
                break;
            default:
                //
                break;
        }

        
    }    
     
    function SetFooterPart1One()
    {
        $this->DataInit = Capsule::table('tbldatainit')->get();
   
        $this->footerPart1One = 
        '
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
            </div>
            <strong>Copyright &copy; '.$this->DataInit[0]->Copyright_tblDataInit.' <a href="'.$this->DataInit[0]->url_tblDataInit.'">'.$this->DataInit[0]->propietario_tblDataInit.'</a>.</strong> All rights
            reserved.
        </footer>
        ';
    }

    function SetFooterPart2One()
    {
        $this->footerPart2One = 
        '
            <!-- jQuery 3 -->
            <script src="bower_components/jquery/dist/jquery.min.js"></script>
            <!-- jQuery UI 1.11.4 -->
            <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
            <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
            <script>
            $.widget.bridge("uibutton", $.ui.button);
            </script>
            <!-- Bootstrap 3.3.7 -->
            <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
            <!-- AdminLTE App -->
            <script src="dist/js/adminlte.min.js"></script>
            <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
            <script src="dist/js/pages/dashboard.js"></script>
            <!-- AdminLTE for demo purposes -->
            <script src="dist/js/demo.js"></script>
        ';
    }

    function getFooterPart1One()
    {
        return  $this->footerPart1One;
    }

    
    function getFooterPart2One()
    {
        return  $this->footerPart2One;
    }
}

?>





