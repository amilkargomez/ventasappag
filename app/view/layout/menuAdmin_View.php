<li class="treeview menu-open">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Menu General OPR</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: block;">
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Menu Admin
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Admin Usuarios</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Procesos
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>