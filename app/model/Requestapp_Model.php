<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Requestapp_Model extends Model {

    protected $primaryKey = 'id_tblrequestapp';
    protected $table = 'tblrequestapp';    

}