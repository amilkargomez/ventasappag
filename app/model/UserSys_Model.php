<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserSys_Model extends Model {

    protected $primaryKey = 'id_tblusersystem';
    protected $table = 'tblusersystem';    

}