<?php

//Debug PHP
//if (getenv('DEBUG') === 'true') {
    ini_set('display_errors', 1);
    ini_set('display_starup_error', 1);
    error_reporting(E_ALL);
//}else{
    //cero debug inabilitar
//}

require_once (dirname(__FILE__,2).'/vendor/autoload.php');

use Aura\Router\RouterContainer;
$routerContainer = new RouterContainer();

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,$_GET,$_POST,$_COOKIE,$_FILES
);

$map = $routerContainer->getMap();

$map->get('index', '/', '../app/view/login/login_View.php');
$map->get('db', '/db', '../app/database/dS.php');
$map->post('indexDashBoardPost', '/dashboard', [
    'controller' => 'App\Controllers\Indexs_Controller',
    'action' => 'validDashboard']);
$map->get('indexDashBoardGet', '/dashboard', [
    'controller' => 'App\Controllers\Indexs_Controller',
    'action' => 'validDashboard']);
$map->get('registrarUsuario', '/registrarUser', [
    'controller' => 'App\Controllers\Register_Controller',
    'action' => 'registrarHome']);
$map->post('saveUsers', '/saveUser', [
    'controller' => 'App\Controllers\Register_Controller',
    'action' => 'registrarUsuario']);
$map->get('saveUsersok', '/regitersok', '../app/view/login/login_View.php');  


$matcher = $routerContainer->getMatcher();
$route = $matcher->match($request);

function routerMachine ($route,$request)
{
    $handlerData = $route->handler;
    $controllerName = $handlerData['controller'];
    $actionName = $handlerData['action'];
    $controller = new $controllerName;
    return $controller->$actionName($request);
}

if (!$route) {
    echo 'No route';
} else {

    switch ($route->name) {

        case 'index':
            require $route->handler;
            break;
        case 'saveUsersok':
            require $route->handler;
            break;
        case 'db':
            require $route->handler;
            break;
        case 'indexDashBoardGet':
            routerMachine($route,$request);
            break;
        case 'indexDashBoardPost':
            routerMachine($route,$request);
            break;
        case 'registrarUsuario':
            routerMachine($route,$request);
            break;
        case 'saveUsers':
            routerMachine($route,$request);
            break;
        case 'validhs':
            //routerMachine($route,$request);
            break;
        default:
            # code...
            break;
    }
}
?>