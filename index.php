<?php

$url = $_SERVER["HTTP_HOST"];

if ( ($url == 'ventasappag.test') || ($url == 'ventasappag.herokuapp.com') ){

    if ($_REQUEST) {
        //Sin Parametros
    }else{

	$ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP'])){
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	}else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else if(isset($_SERVER['HTTP_X_FORWARDED'])){
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    }else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])){
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    }else if(isset($_SERVER['HTTP_FORWARDED_FOR'])){
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    }else if(isset($_SERVER['HTTP_FORWARDED'])){
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    }else if(isset($_SERVER['REMOTE_ADDR'])){
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    }else{
		$ipaddress = 'UNKNOWN';
	}

	$idRamd = rand(5, 88888);

	$_REQUEST = array('PATH' => $_SERVER['PATH'], 'IPCLIENT' => $ipaddress, 
						'REQUEST_TIME' => $_SERVER['REQUEST_TIME'], 
							'ID_CLIENT' => $idRamd);

    }

	include (dirname(__FILE__,2).'/app/router.php');
	
}else{
	echo 'Router_PHP_NO_CONFIGURADO';
}

?>